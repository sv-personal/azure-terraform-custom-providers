terraform {
  required_providers {
    # https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "3.18.0"
    }
  }
}

provider "azurerm" {
  features {}
  alias           = "pay-as-you-go"
  tenant_id       = var.tenant_id
  subscription_id = var.payg_subscription_id
  client_id       = var.payg_client_id
  client_secret   = var.payg_client_secret
}

provider "azurerm" {
  features {}
  alias           = "subscription-b"
  tenant_id       = var.tenant_id
  subscription_id = var.subscription_b_subscription_id
  client_id       = var.subscription_b_client_id
  client_secret   = var.subscription_b_client_secret
}
