# Azure Terraform Custom Providers

## Goals

- [x] Create a second Azure Subscription
- [x] [Create App Registrations with appropriate RBAC permissions for Terraform to operate](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/guides/service_principal_client_secret#creating-a-service-principal-in-the-azure-portal). Details in Dashlane/_azure-terraform-providers-clients
- [x] Create local Environment Variables (TF_VAR_ prefix) to house the below for each Subscription:
  - Tenant ID
  - Subscription ID
  - Client ID
  - Client Secret
- [x] [Add custom Provider blocks](https://www.terraform.io/language/providers/configuration#default-provider-configurations) to `versions.tf` in repo, specifying the below for each subscription - (variables
  to map to the above-created ENV vars, further declared in `variables.tf`, respectively)
  - Tenant ID
  - Subscription ID
  - Client ID
  - Client Secret
- [x] Use the Provider aliases to deploy some simple resources in each Subscription

## Stretch

- [ ] Build a simple CI/CD pipeline to run the Terraform
- [ ] Integrate the variables required for the custom Providers into CI/CD variables
  - TF_VARS_tenant_id <--- (if we were targeting multiple Tenants we'd have more of these)
  - TF_VARS_sub_a_subscription_id
  - TF_VARS_sub_b_subscription_id
  - TF_VARS_sub_a_client_id
  - TF_VARS_sub_b_client_id
  - and so on... ENSURING TO MASK THE CLIENT SECRETS
- [ ] Update Terraform code to use these CI/CD variables

## Name
Azure Terraform Custom Providers

## Description
Demonstrating the use of custom Provider blocks in Terraform to allow creation of resources in multiple Azure Subscriptions / Tenants from a single Terraform code repository.

## License
For open source projects, say how it is licensed.
