# Simple Resource Group in both subscriptions
resource "azurerm_resource_group" "payg-rg-1" {
    provider = azurerm.pay-as-you-go
    name = "custom-providers-payg-rg"
    location = var.location
}

resource "azurerm_resource_group" "subscription_b-rg-1" {
    provider = azurerm.subscription-b
    name = "custom-providers-subscription_b-rg"
    location = var.location
}
