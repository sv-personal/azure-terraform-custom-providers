variable "tenant_id" {
  description = "Unique ID of the Azure Tenant"
}

variable "payg_client_id" {
  description = "Unique ID of the Pay-as-you-Go Authorised Enterprise App"
}

variable "payg_client_secret" {
  description = "Password of the Pay-as-you-Go Authorised Enterprise App"
}

variable "payg_subscription_id" {
  description = "Unique ID of the Pay-as-you-Go Subscription"
}

variable "subscription_b_client_id" {
  description = "Unique ID of the Subscription_B Authorised Enterprise App"
}

variable "subscription_b_client_secret" {
  description = "Password of the Subscription_B Authorised Enterprise App"
}

variable "subscription_b_subscription_id" {
  description = "Unique ID of the Subscription_B Subscription"
}

variable "location" {
  default = "uksouth"
}
